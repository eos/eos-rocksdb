#-------------------------------------------------------------------------------
# Relocate the eos-rocksdb provided libraries to /opt/eos/rocksdb/
#-------------------------------------------------------------------------------
%define _prefix /opt/eos/rocksdb/
%define _unpackaged_files_terminate_build 0

Name:           eos-rocksdb
Summary:        A library that provides an embeddable, persistent key-value store for fast storage.
Version:        8.8.1
Release:        1%{dist}%{?_with_asan:.asan}%{?_with_tsan:.tsan}
License:        Apache
URL:            https://github.com/facebook/rocksdb.git
Source0:        https://github.com/facebook/rocksdb/archive/v%{version}.tar.gz

BuildRequires: gcc-c++
BuildRequires: make
BuildRequires: which
BuildRequires: zlib-static
BuildRequires: zlib-devel
BuildRequires: libzstd-devel
BuildRequires: lz4-devel
BuildRequires: snappy-devel
Requires: libzstd
Requires: lz4
Requires: snappy

# For asan/tsan builds we need devtoolset-9
%if %{?_with_asan:1}%{!?_with_asan:0} || %{?_with_tsan:1}%{!?_with_tsan:0}
%define devtoolset devtoolset-9
%else
%define devtoolset devtoolset-8
%endif

%if 0%{?rhel} == 7
BuildRequires: %{devtoolset}-gcc-c++
%endif

%if %{?_with_asan:1}%{!?_with_asan:0}
%if 0%{?rhel} == 7
BuildRequires: libasan5, %{devtoolset}-libasan-devel
Requires: libasan5
%else
BuildRequires: libasan
Requires: libasan
%endif
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
%if 0%{?rhel} == 7
BuildRequires: libtsan, %{devtoolset}-libtsan-devel
Requires: libtsan
%else
BuildRequires: libtsan
Requires: libtsan
%endif
%endif


%description
Rocksdb is a library that forms the core building block for a fast key value
server, especially suited for storing data on flash drives. It has a
Log-Structured-Merge-Database (LSM) design with flexible trade offs between
Write-Amplification-Factor (WAF), Read-Amplification-Factor (RAF) and
Space-Amplification-Factor (SAF). It has multithreaded compaction, making it
specially suitable for storing multiple terabytes of data in a single database.

%global debug_package %{nil}

%package devel
Summary: rocksdb development files
Group: Development/Libraries

%description devel
This package provides the headers and static library for rocksdb.

%prep
%setup -n rocksdb-%{version}

%build
export PORTABLE=1
export DISABLE_JEMALLOC=1
export OPT='-fPIC -DNDEBUG -O3'

%if %{?_with_asan:1}%{!?_with_asan:0}
export CXXFLAGS="${CXXFLAGS} -fsanitize=address -Wno-error=deprecated-copy -Wno-error=pessimizing-move -Wno-error=maybe-uninitialized"
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
export CXXFLAGS="${CXXFLAGS} -fsanitize=thread -Wno-error=deprecated-copy -Wno-error=pessimizing-move -Wno-error=maybe-uninitialized"
%endif

%if 0%{?rhel} >= 8
export CXXFLAGS="${CXXFLAGS} -Wno-error=unused-function"
%endif

%if 0%{?fedora:1} || 0%{?rhel} == 9
# For Fedora Rawhide remove some warning flags
%if 0%{?fedora} >= 40
export CFLAGS="${CFLAGS//-Werror=implicit-function-declaration/}"
export CFLAGS="${CFLAGS//-Werror=implicit-int/}"
%endif
export CXXFLAGS="${CXXFLAGS} -Wno-error=deprecated-copy -Wno-error=pessimizing-move -Wno-error=range-loop-construct -Wno-error=unused-function -Wno-error=restrict"
%endif

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

make %{?_smp_mflags} static_lib tools_lib USE_RTTI=1 DEBUG_LEVEL=0

%install
%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

install -d %{buildroot}/%{_includedir}
cd include

for header_dir in `find "rocksdb" -type d`; do
  install -d %{buildroot}/%{_includedir}/${header_dir};
done

for header in `find "rocksdb" -type f -name *.h`; do
  install -C -m 644 "${header}" "%{buildroot}/%{_includedir}/${header}"
done

cd ..
install -d %{buildroot}/%{_libdir}
install -C -m 755 librocksdb.a %{buildroot}/%{_libdir}/librocksdb.a
install -C -m 755 librocksdb_tools.a %{buildroot}/%{_libdir}/librocksdb_tools.a

%files
%{_libdir}/librocksdb.a
%{_libdir}/librocksdb_tools.a
%{_includedir}/rocksdb/*

%changelog
* Thu Sep 28 2017 Georgios Bitzes <georgios.bitzes@cern.ch> - 5.7.3
- Initial package
